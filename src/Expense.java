import java.util.Date;

public class Expense {
    private Currency currency;
    private double amount;
    private String description;
    private String location;
    private Date date;
    private String paidBy;

    public Expense(Currency currency, double amount, String description, String location, Date date, String paidBy) {
        this.currency = currency;
        this.amount = amount;
        this.description = description;
        this.location = location;
        this.date = date;
        this.paidBy = paidBy;
    }

    public Date getDate() {
        return date;
    }

    public double getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public String getPaidBy() {
        return paidBy;
    }
}