import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class TravelExpenseManagementSolution {
    private static String[] splitQuery(String query){
        return query.split("\\s+");
    }

    private static String[] getExpense(String input) {
        String[] processedInput = input.substring(3).split(",");
        if (processedInput.length <= 5) {
            processedInput[5]="Self";
        }
        return processedInput;
    }

    public static void main(String args[]) throws IOException, ParseException {
        char choice='Y';
        String input;
        BufferedReader inputReader=new BufferedReader((new InputStreamReader(System.in)));
        User user = new User("Vivek");
        while (choice=='Y'){
            System.out.println("Enter the expense:");
            input=inputReader.readLine();
            String[] expense= getExpense(input);
            user.spend(Double.valueOf(expense[1]), Currency.valueOf(expense[0]), new SimpleDateFormat("yyy-mm-dd").parse(expense[4]), expense[2], expense[5], expense[3]);
            System.out.println("Do you want to continue:");
            choice=inputReader.readLine().charAt(0);
        }
        String query;
        System.out.println("Enter query:");
        query=inputReader.readLine();
        String[] splitQuery = splitQuery(query);
        List<Expense> spendings = user.spendings(new SimpleDateFormat("yyy-mm-dd").parse(splitQuery[3]), Currency.valueOf(splitQuery[5]));
        Map<String, Optional<Double>> dues = user.dues(new SimpleDateFormat("yyy-mm-dd").parse(splitQuery[3]), Currency.valueOf(splitQuery[5]));
        //printSpendings(spendings);
        //printDues(dues);
    }
}

