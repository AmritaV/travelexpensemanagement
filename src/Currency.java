public enum Currency {
    INR(100), USD(2), GBP(1);

    private int conversionFactor;

    Currency(int conversionFactor) {
        this.conversionFactor = conversionFactor;
    }

    public int getConversionFactor() {
        return conversionFactor;
    }
}
