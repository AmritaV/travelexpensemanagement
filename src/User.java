import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class User {
    private String name;
    private List<Expense> expenses;

    public User(String name) {
        this.name = name;
        expenses = new ArrayList<>();
    }

    public void spend(double amount, Currency currency, Date date, String location, String paidBy, String description) {
        expenses.add(new Expense(currency, amount, description, location, date, paidBy));
    }

    public List<Expense> spendings(Date date, Currency currency) {
        return expenses.stream()
                .map(expensesForCurrencyFunction(currency))
                .filter(expenseForDatePredicate(date))
                .filter(e -> e.getPaidBy().equals(this.name))
                .collect(Collectors.toList());
    }

    public Map<String, Optional<Double>> dues(Date date, Currency currency) {
        return expenses.stream()
                .map(expensesForCurrencyFunction(currency))
                .filter(expenseForDatePredicate(date))
                .filter(e -> !e.getPaidBy().equals(this.name))
                .collect(Collectors.groupingBy(Expense::getPaidBy))
                .entrySet().stream().collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue().stream().map(Expense::getAmount).reduce((expense, prev) -> expense + prev)
                ));
    }

    private Function<Expense, Expense> expensesForCurrencyFunction(Currency currency) {
        return e -> new Expense(
                currency,
                e.getAmount() / e.getCurrency().getConversionFactor() * currency.getConversionFactor(),
                e.getDescription(),
                e.getLocation(),
                e.getDate(),
                e.getPaidBy()
        );
    }

    private Predicate<Expense> expenseForDatePredicate(Date date) {
        return e -> e.getDate().compareTo(date) == 0;
    }
}
